<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$container = get_theme_mod( 'understrap_container_type' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div class="site" id="page">

	<!-- ******************* The Navbar Area ******************* -->
	<div id="wrapper-navbar" itemscope itemtype="http://schema.org/WebSite">

		<a class="skip-link sr-only sr-only-focusable" href="#content"><?php esc_html_e( 'Skip to content', 'understrap' ); ?></a>

		<header class="site-header bg-primary">

		<?php if ( 'container' == $container ) : ?>
			<div class="container">
		<?php endif; ?>

			<div class="row no-gutters">

				<div class="col-4 d-md-none navbar-toggler collapsed" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="<?php esc_attr_e( 'Toggle navigation', 'understrap' ); ?>">
					<span class="nav-toggler"></span>
				</div>

				<div class="col-4 col-md-10 text-center text-md-left">
					<!-- Your site title as branding in the menu -->
					<?php if ( ! has_custom_logo() ) { ?>

					<?php if ( is_front_page() && is_home() ) : ?>

						<h1 class="mb-0"><a rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" itemprop="url"><img class="logo" src="<?php echo  get_stylesheet_directory_uri(); ?>/images/logo.svg" /></a> <a class="brand" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" itemprop="url"><?php bloginfo( 'name' ); ?></a></h1>

					<?php else : ?>

						<a rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" itemprop="url"><img class="logo" src="<?php echo  get_stylesheet_directory_uri(); ?>/images/logo.svg" /></a>
						<a class="brand "mb-0"" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" itemprop="url"><?php bloginfo( 'name' ); ?></a>

					<?php endif; ?>

					<?php } else {
					the_custom_logo();
					?>
					<a class="brand" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" itemprop="url"><?php bloginfo( 'name' ); ?></a>

					<?php } ?>
					<!-- end custom logo -->
				</div>

				<div class="col-4 col-md-2 text-right search-toggler collapsed" data-toggle="collapse" data-target="#search-box" aria-controls="search-box" aria-expanded="false" >
					<div class="search-toggle-button"></div>
				</div>

			</div><!-- .row -->

		<?php if ( 'container' == $container ) : ?>
			</div><!-- .container -->
		<?php endif; ?>

		</header>


		<div class="search-box-wrapper">
		<?php if ( 'container' == $container ) : ?>
			<div class="container">
		<?php endif; ?>

			<div id="search-box" class="search-box collapse">
				<?php get_search_form(); ?>
			</div>

		<?php if ( 'container' == $container ) : ?>
		</div><!-- .container -->
		<?php endif; ?>
		</div>


		<nav class="navbar navbar-expand-md navbar-dark bg-secondary">

		<?php if ( 'container' == $container ) : ?>
			<div class="container">
		<?php endif; ?>

			<!-- The WordPress Menu goes here -->
			<?php wp_nav_menu(
				array(
					'theme_location'  => 'primary',
					'container_class' => 'collapse navbar-collapse',
					'container_id'    => 'navbarNavDropdown',
					'menu_class'      => 'navbar-nav',
					'fallback_cb'     => '',
					'menu_id'         => 'main-menu',
					'depth'           => 2,
					'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
				)
			); ?>

		<?php if ( 'container' == $container ) : ?>
			</div><!-- .container -->
		<?php endif; ?>

		</nav><!-- .site-navigation -->




	</div><!-- #wrapper-navbar end -->
