<?php
/**
 * Post rendering content according to caller of get_template_part.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

	<div class="row">

		<div class="col-12 col-md-8">
			<?php echo get_the_post_thumbnail( $post->ID, 'full' ); ?>
		</div>

		<div class="col-12 col-md-4">
			<header class="entry-header">

				<?php if ( 'post' == get_post_type() ) : ?>

				<div class="entry-meta">
					<?php the_date(); ?>
				</div><!-- .entry-meta -->

				<?php endif; ?>

				<?php
				the_title(
					sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ),
					'</a></h2>'
				);
				?>

			</header><!-- .entry-header -->

			<div class="entry-content">

				<?php the_excerpt(); ?>

			</div><!-- .entry-content -->
		</div>

	</div><!-- .row -->

</article><!-- #post-## -->
