<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

function understrap_remove_scripts() {
    wp_dequeue_style( 'understrap-styles' );
    wp_deregister_style( 'understrap-styles' );

    wp_dequeue_script( 'understrap-scripts' );
    wp_deregister_script( 'understrap-scripts' );

    // Removes the parent themes stylesheet and scripts from inc/enqueue.php
}
add_action( 'wp_enqueue_scripts', 'understrap_remove_scripts', 20 );

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {

    // Get the theme data
    $the_theme = wp_get_theme();
    wp_enqueue_style( 'child-understrap-styles', get_stylesheet_directory_uri() . '/css/child-theme.min.css', array(), $the_theme->get( 'Version' ) );
    wp_enqueue_script( 'jquery');
    wp_enqueue_script( 'child-understrap-scripts', get_stylesheet_directory_uri() . '/js/child-theme.min.js', array(), $the_theme->get( 'Version' ), true );
    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}

function add_child_theme_textdomain() {
    load_child_theme_textdomain( 'understrap-child', get_stylesheet_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'add_child_theme_textdomain' );


//Modify Search placeholder
function wpforo_search_form( $html ) {
    $html = str_replace( 'placeholder="Search ', 'placeholder="e.g delicious sandwiches ', $html );

    return $html;
}
add_filter( 'get_search_form', 'wpforo_search_form' );


//Modify the excerpt
function child_theme_setup() {
    // override parent theme's 'more' text for excerpts
    remove_filter( 'excerpt_more', 'understrap_custom_excerpt_more' ); 
    remove_filter( 'wp_trim_excerpt', 'understrap_all_excerpts_get_more_link' );
}
add_action( 'after_setup_theme', 'child_theme_setup' );

add_filter( 'wp_trim_excerpt', 'understrap_all_excerpts_get_more_link2' );
    if ( ! function_exists( 'understrap_all_excerpts_get_more_link2' ) ) {
    /**
     * Adds a custom read more link to all excerpts, manually or automatically generated
     *
     * @param string $post_excerpt Posts's excerpt.
     *
     * @return string
     */
    function understrap_all_excerpts_get_more_link2( $post_excerpt ) {
        $post_excerpt = $post_excerpt . '<a class="read-more" href="' . esc_url( get_permalink( get_the_ID() ) ) . '">' . __( 'Read More','understrap' ) . '</a>';
        return $post_excerpt;
    }
}
