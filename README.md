# Eats WP Theme

## Installation
1. Upload the parent theme UnderStrap first: `https://github.com/understrap/understrap`
1. Upload the Eats folder to your wp-content/themes directory
1. Go to "Appearance -> Themes" in the WP backend
1. Activate the Eats theme

## Style Editing
`/sass/theme/_child_theme.scss`
`/sass/theme/_child_theme_variables.scss`

## Installing Dependencies
- Make sure you have installed Node.js, Gulp, and Browser-Sync [1] on your computer globally
- Open your terminal and browse to the location of your UnderStrap copy
- Run: `$ npm install` then: `$ gulp copy-assets`

## Running
To work and compile your Sass files on the fly start:
- `$ gulp watch`
- `$ gulp watch-bs`